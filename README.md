# ndarray-xcorr2

[![pipeline status](https://gitlab.com/daviortega/ndarray-xcorr2/badges/master/pipeline.svg)](https://gitlab.com/daviortega/ndarray-xcorr2/commits/master)
[![coverage report](https://gitlab.com/daviortega/ndarray-xcorr2/badges/master/coverage.svg)](https://gitlab.com/daviortega/ndarray-xcorr2/commits/master)

2d cross correlation matrix based on ndarrays.

This program is largely inspired by [phase-align](https://github.com/scijs/phase-align). It works with 2x2 matrices in ndarray format.


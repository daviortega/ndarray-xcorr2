'use strict'

const expect = require('chai').expect

const pack = require('ndarray-pack')
const xcorr2 = require('./ndarray-xcorr2')
const getPixels = require('get-pixels')
const ops = require('ndarray-ops')
const test = require('ndarray-tests')

describe('xcorr2', function() {
	describe('simple tests', function() {
		it('should give the right answer for a 2D (3x3) matrix', function() {
			const matrix1 = pack([
				[1, 0, 0],
				[0, 0, 0],
				[0, 0, 0]
			])
			const matrix2 = pack([
				[1, 0, 0],
				[0, 0, 0],
				[0, 0, 0]
			])
			const expected = pack([
				[0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0],
				[0, 0, 1, 0, 0],
				[0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0]
			])
			const sol = xcorr2(matrix1, matrix2)
			expect(test.approximatelyEqual(sol, expected, 1e-6)).to.be.true
		})
		it('should give the right answer with 2x2 matrices', function() {
			const matrix1 = pack([
				[0, 1],
				[0, 0]
			])
			const matrix2 = pack([
				[1, 0],
				[0, 0]
			])
			const expected = pack([
				[0, 0, 0],
				[0, 0, 1],
				[0, 0, 0]
			])
			const sol = xcorr2(matrix1, matrix2)
			expect(test.approximatelyEqual(sol, expected, 1e-6)).to.be.true
		})
		it('should give the right answer 3x3 case 1', function() {
			const matrix1 = pack([
				[0, 1, 0],
				[0, 0, 0],
				[0, 0, 0]
			])
			const matrix2 = pack([
				[1, 0, 0],
				[0, 0, 0],
				[0, 0, 0]
			])
			const expected = pack([
				[0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0],
				[0, 0, 0, 1, 0],
				[0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0]
			])
			const sol = xcorr2(matrix1, matrix2)
			expect(test.approximatelyEqual(sol, expected, 1e-6)).to.be.true
		})
		it('should give the right answer 3x3 case 2', function() {
			const matrix1 = pack([
				[0, 1, 0],
				[0, 0, 0],
				[0, 0, 0]
			])
			const matrix2 = pack([
				[1, 0, 0],
				[0, 0, 1],
				[0, 0, 1]
			])
			const expected = pack([
				[0, 1, 0, 0, 0],
				[0, 1, 0, 0, 0],
				[0, 0, 0, 1, 0],
				[0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0]
			])
			const sol = xcorr2(matrix1, matrix2)
			expect(test.approximatelyEqual(sol, expected, 1e-6)).to.be.true
		})
		it('should give the right answer 5x3 vs 4x3', function() {
			const matrix1 = pack([
				[0, 1, 0, 0, 0],
				[0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0]
			])
			const matrix2 = pack([
				[1, 0, 0, 0],
				[0, 0, 1, 0],
				[0, 0, 1, 0]
			])
			const expected = pack([
				[0, 0, 1, 0, 0, 0, 0, 0],
				[0, 0, 1, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 1, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0],
				[0, 0, 0, 0, 0, 0, 0, 0]
			])
			const sol = xcorr2(matrix1, matrix2)
			expect(test.approximatelyEqual(sol, expected, 1e-6)).to.be.true
		})
	})
	describe('tests with non-squared matrices', function() {
		it('should give the same answer as the xcorr2 from matlab', function() {
			const matrix1 = pack([
				[1, 1, 1],
				[1, 1, 1]
			])
			const matrix2 = pack([
				[1, 2],
				[3, 4],
				[5, 6]
			])
			const expected = pack([
				[6, 11, 11, 5],
				[10, 18, 18, 8],
				[6, 10, 10, 4],
				[2, 3, 3, 1]
			])
			const sol = xcorr2(matrix1, matrix2)
			expect(test.approximatelyEqual(sol, expected, 1e-6)).to.be.true
		})
		it('should give the same answer as the xcorr2 from matlab', function() {
			const matrix1 = pack([
				[1, 1],
				[1, 1],
				[1, 1]
			])
			const matrix2 = pack([
				[1, 2, 3],
				[4, 5, 6]
			])
			const expected = pack([
				[6, 11, 9, 4],
				[9, 16, 12, 5],
				[9, 16, 12, 5],
				[3, 5, 3, 1]
			])
			const sol = xcorr2(matrix1, matrix2)
			expect(test.approximatelyEqual(sol, expected, 1e-6)).to.be.true
		})
	})
	describe('semi real world tests', function() {
		it('should work with small images', function(done) {
			getPixels('test-data/small1.png', (err, matrix1) => {
				if (err)
					throw err
				getPixels('test-data/small2.png', (error, matrix2) => {
					if (error)
						throw err
					getPixels('test-data/xcorr.small1x2.ref.png', (error2, expected) => {
						if (error2)
							throw error2
						const m1 = matrix1.pick(null, null, 3)
						const m2 = matrix2.pick(null, null, 3)
						const m3 = expected.pick(null, null, 0)
						const sol = xcorr2(m1, m2)
						const sup = ops.sup(sol)
						ops.divseq(sol, sup)
						ops.mulseq(sol, 255)
						expect(test.approximatelyEqual(sol, m3, 1)).to.be.true
						done()
					})
				})
			})
		})
		it('should work with medium images', function(done) {
			getPixels('test-data/image1.png', (err, matrix1) => {
				if (err)
					throw err
				getPixels('test-data/image3.png', (error, matrix2) => {
					if (error)
						throw err
					getPixels('test-data/xcorr.1.3.ref.png', (error2, expected) => {
						if (error2)
							throw error2
						const m1 = matrix1.pick(null, null, 3)
						const m2 = matrix2.pick(null, null, 3)
						const m3 = expected.pick(null, null, 0)
						const sol = xcorr2(m1, m2)
						const sup = ops.sup(sol)
						ops.divseq(sol, sup)
						ops.mulseq(sol, 255)
						expect(test.approximatelyEqual(sol, m3, 1)).to.be.true
						done()
					})
				})
			})
		})
		it('should work with medium images of different sizes', function(done) {
			this.timeout(60000)
			getPixels('test-data/image1.png', (err, matrix1) => {
				if (err)
					throw err
				getPixels('test-data/image6.png', (error, matrix2) => {
					if (error)
						throw err
					getPixels('test-data/xcorr.1.6.ref.png', (error2, expected) => {
						if (error2)
							throw error2
						const m1 = matrix1.pick(null, null, 3)
						const m2 = matrix2.pick(null, null, 3)
						const m3 = expected.pick(null, null, 0)
						const sol = xcorr2(m1, m2)
						const sup = ops.sup(sol)
						ops.divseq(sol, sup)
						ops.mulseq(sol, 255)
						expect(test.approximatelyEqual(sol, m3, 1)).to.be.true
						done()
					})
				})
			})
		})
	})
})

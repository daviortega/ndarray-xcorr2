'use strict'

const ndarray = require('ndarray')
const fft = require('ndarray-fft')
const pool = require('typedarray-pool')
const ops = require('ndarray-ops')
const cops = require('ndarray-complex')
const translate = require('ndarray-translate-fft')

module.exports = (a, b) => {
	if (a.shape.length !== b.shape.length)
		throw new Error('Images must be the same dimensions!')

	let d = a.shape.length
	let nshape = new Array(d)
	let nstride = new Array(d)
	let i = 1
	let s = 1

	for (i = 0; i < d; ++i) {
		nshape[i] = a.shape[i] + b.shape[i] - 1
		nstride[i] = s
		s *= nshape[i]
	}

	let axT = pool.mallocDouble(s)
	let ax = ndarray(axT, nshape, nstride, 0)
	let ayT = pool.mallocDouble(s)
	let ay = ndarray(ayT, nshape, nstride, 0)
	let bxT = pool.mallocDouble(s)
	let bx = ndarray(bxT, nshape, nstride, 0)
	let byT = pool.mallocDouble(s)
	let by = ndarray(byT, nshape, nstride, 0)

	ops.assigns(ax, 0)
	ops.assigns(ay, 0)
	ops.assign(ax.hi.apply(ax, a.shape), a)
	fft(1, ax, ay)

	ops.assigns(bx, 0)
	ops.assign(bx.hi.apply(bx, b.shape), b)
	ops.assigns(by, 0)
	fft(1, bx, by)

	ops.negeq(ay)

	cops.muleq(ax, ay, bx, by)
	fft(-1, ax, ay)

	pool.freeDouble(axT)
	pool.freeDouble(ayT)
	pool.freeDouble(bxT)
	pool.freeDouble(byT)

	let M = a.shape[0]
	let N = a.shape[1]

	ax = ax.step(-1, -1)
	translate.wrap(ax, a.shape.map((j) => 1 - j))

	return ax
}